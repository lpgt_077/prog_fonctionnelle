package org.example.Interfaces;

import java.util.List;

interface Affichable
{
    void display (int nb);
}

class AfficheInt implements Affichable
{
    @Override
    public void display(int nb)
    {
        System.out.println(nb);
    }
}

class AfficheIntLigne implements Affichable
{
    @Override
    public void display(int nb)
    {
        System.out.print(nb + " | ");
    }
}

public class Ex_1_3
{

    public static void main(String[] args)
    {
        //log.info("Hello world!");
        List<Integer> nombres = List.of(12, 9, 13, 4, 6, 2, 4, 12 , 15);

        Affichable afficheur = new AfficheInt();
        Affichable afficheLigne = new AfficheIntLigne();

        afficherListe(nombres, afficheLigne);
        System.out.println("\n----------------------");
        afficherListePairs(nombres, afficheur);
        //log.info("{}", e);
    }

    public static void afficherListePairs(List<Integer> liste, Affichable aff)
    {
        for (Integer nombre : liste)
        {
            if (nombre%2 == 0)
            {
               aff.display(nombre);
            }

        }
    }
    public static void afficherListe(List<Integer> liste, Affichable aff)
    {
        for (Integer nombre : liste)
        {
            aff.display(nombre);
        }
    }



}
