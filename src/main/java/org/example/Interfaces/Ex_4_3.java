package org.example.Interfaces;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;


class getStars implements Function<Integer, String>
{
    public String apply(Integer nb)
    {
        String chaine = "*";
        return chaine.repeat(nb);
    }

}


public class Ex_4_3
{

    public static void main(String[] args)
    {

        Supplier<List<Integer>> listSupplier = () -> List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        getStars getStars = new getStars();
        List<Integer> nombres = listSupplier.get();
        nombres.stream()
                .map(getStars)
                .forEach(System.out::println);
    }




}
