package org.example.Interfaces;

import java.util.List;

public class Ex_2_1
{

    public static void main(String[] args)
    {
        List<Integer> nombres = List.of(12, 9, 13, 4, 6, 2, 4, 12 , 15);
        // :: --> reference a la méthode println (= pointeur fct en C)
        nombres.stream().forEach(System.out::println);

        /*
        Collection.forEach() utilise l'itérateur de la collection (si celui-ci est spécifié),
        ainsi l'ordre de traitement des éléments est défini.
        En revanche, l'ordre de traitement de Collection.stream().forEach() n'est pas défini.
        https://www.baeldung.com/java-collection-stream-foreach
         */
        nombres.forEach(System.out::println);
    }

}
