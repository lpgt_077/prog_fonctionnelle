package org.example.Interfaces;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;


class display implements Consumer<Integer>
{
    @Override
    public void accept(Integer integer) {
        System.out.println(integer);
    }
}


public class Ex_4_1
{

    public static void main(String[] args)
    {

        Supplier<List<Integer>> listSupplier = () -> List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        display display = new display();
        List<Integer> nombres = listSupplier.get();
        nombres.stream()
                .map(getInteger())
                .forEach(display);
    }

    private static Function<Integer, Integer> getInteger() {
        return nb -> nb * 2;
    }


}
