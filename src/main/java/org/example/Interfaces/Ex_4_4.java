package org.example.Interfaces;


import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Stream;


public class Ex_4_4
{

    public static void main(String[] args)
    {
       // Supplier<Integer> randomSupplier = new Random()::nextInt;

        Supplier<Integer> randomSupplier = () -> (int)(Math.random()*100);
        Stream.generate(randomSupplier)
                .limit(10)
                .forEach(System.out::println);

        /* 2ème méthode :
        Stream<Integer> stream = Stream.generate(() -> (int)(Math.random()*100))
                .limit(10);
        stream.forEach(System.out::println);
        */


    }


}
