package org.example.Interfaces;

import java.util.List;

public class Ex_2_3
{
    public static void display(Integer x)
    {
        System.out.println(x);
    }

    private static void print(Integer x)
    {
        display(x);
    }

    private static boolean isEven(Integer nb)
    {
        return nb%2==0;
    }

    public static void main(String[] args)
    {
        List<Integer> nombres = List.of(12, 9, 13, 4, 6, 2, 4, 12 , 15);
        nombres.stream()
                // Syntaxe Ex_2_3::isEven mieux que nb -> isEven(nb)
                .filter(Ex_2_3::isEven)
                .forEach(Ex_2_3::print);
    }

}
