package org.example.Interfaces;

import java.util.List;


public class Ex_1_2
{
    private static List<Integer> nombres;

    public static void main(String[] args)
    {
        //log.info("Hello world!");
        List<Integer> nombres = List.of(12, 9, 13, 4, 6, 2, 4, 12 , 15);
        store(nombres);
        afficherNbPaire();
        //log.info("{}", e);
    }

    public static void store(List<Integer> nbs)
    {
        nombres = nbs;
    }

    public static void afficherNbPaire()
    {
        for (Integer nombre : nombres)
        {
            if (nombre%2 == 0)
            {
                System.out.println(nombre);
            }

        }
    }

}
