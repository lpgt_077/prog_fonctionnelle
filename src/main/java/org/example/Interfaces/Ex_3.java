package org.example.Interfaces;

import java.util.List;

public class Ex_3
{

    public static void main(String[] args)
    {
        List<String> months = List.of("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");
        // :: --> reference a la méthode println (= pointeur fct en C)
        // filter : nb (eq params passé a une fonction) / nb%2==0 --> eq return nb%2==0; dans une fct() permettant
        // ensuite d'appliquer le filtre en fonction du resultat (true ou false)
        months.stream().filter(month -> month.contains("bre")).forEach(System.out::println);
    }

}
