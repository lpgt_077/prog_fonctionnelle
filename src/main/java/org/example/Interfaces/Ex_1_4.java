package org.example.Interfaces;

import java.util.List;

interface AffichableGeneric<t>
{

    void display (t x);
}

class AfficheInteger implements AffichableGeneric<Integer>
{
    @Override
    public void display(Integer x)
    {
        System.out.println(x);
    }
}

class AfficheIntegerLigne implements AffichableGeneric<Integer>
{
    @Override
    public void display(Integer x)
    {
        System.out.print(x + " | ");
    }
}

public class Ex_1_4
{

    public static void main(String[] args)
    {
        //log.info("Hello world!");
        List<Integer> nombres = List.of(12, 9, 13, 4, 6, 2, 4, 12 , 15);

        AffichableGeneric<Integer> afficheur = new AfficheInteger();
        AffichableGeneric<Integer> afficheLigne = new AfficheIntegerLigne();

        afficherListe(nombres, afficheLigne);
        System.out.println("\n----------------------");
        afficherListePairs(nombres, afficheur);
        //log.info("{}", e);
    }

    public static void afficherListePairs(List<Integer> liste, AffichableGeneric<Integer> aff)
    {
       // String type = liste.getClass().getField();
        // System.out.println(type);

        for (Integer nombre : liste)
        {
            if (nombre%2 == 0)
            {
               aff.display(nombre);
            }

        }
    }
    public static void afficherListe(List<Integer> liste, AffichableGeneric<Integer> aff)
    {
        for (Integer nombre : liste)
        {
            aff.display(nombre);
        }
    }



}
