package org.example.Interfaces;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;


class display2 implements Consumer<Integer>
{
    @Override
    public void accept(Integer integer) {
        System.out.println(integer);
    }
}

class multiplierEntier implements Function<Integer, Integer>
{
    public Integer apply(Integer nb) {
        return nb*2;
    }

}


public class Ex_4_2
{

    public static void main(String[] args)
    {

        Supplier<List<Integer>> listSupplier = () -> List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        display2 display = new display2();
        multiplierEntier multiplierEntier = new multiplierEntier();
        List<Integer> nombres = listSupplier.get();
        nombres.stream()
                .map(multiplierEntier)
                .forEach(display);
    }




}
