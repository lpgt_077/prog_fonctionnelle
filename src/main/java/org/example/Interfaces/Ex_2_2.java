package org.example.Interfaces;

import java.util.List;

public class Ex_2_2
{

    public static void main(String[] args)
    {
        List<Integer> nombres = List.of(12, 9, 13, 4, 6, 2, 4, 12 , 15);
        // :: --> reference a la méthode println (= pointeur fct en C)
        // filter : nb (eq params passé a une fonction) / nb%2==0 --> eq return nb%2==0; dans une fct() permettant
        // ensuite d'appliquer le filtre en fonction du resultat (true ou false)
        nombres.stream().filter(nb -> nb%2==0).forEach(System.out::println);
    }

}
