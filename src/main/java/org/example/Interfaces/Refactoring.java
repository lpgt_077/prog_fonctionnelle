package org.example.Interfaces;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;


class displayConsumer implements Consumer<Integer>
{
    @Override
    public void accept(Integer integer) {
        System.out.println(integer);
    }
}
class PairePredicate implements  Predicate<Integer>
{
    public boolean test(Integer nb) {
        return nb % 2 == 0;
    }

}

public class Refactoring
{

    public static void main(String[] args)
    {
        int x = 67;
        System.out.println("calcul : " + calcul(x));

        Supplier<List<Integer>> listSupplier = () -> List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        PairePredicate estPair = new PairePredicate();
        displayConsumer display = new displayConsumer();
        List<Integer> nombres = listSupplier.get();
        nombres.stream()
                .filter(estPair)
                .forEach(display);
    }



    private static int calcul(int x)
    {
        return 3 * x + 5;
    }

}
