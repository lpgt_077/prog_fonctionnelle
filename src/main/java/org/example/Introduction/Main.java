package org.example.Introduction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@ToString
@Getter
class Eleve
{
    private String nom;
    @Setter
    private double note;
}

@Slf4j
public class Main
{
    public static void main(String[] args)
    {
        log.info("Hello world!");
        Eleve e = new Eleve("Lepage Tom", 18);
        e.setNote(13);
        log.info("{}", e);
    }
}