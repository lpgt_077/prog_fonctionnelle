package org.example.Traitements;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@Getter
public
class Enseignement
{
    private String titre;
    private int duree;
    private int coef;
    private String ue;
}