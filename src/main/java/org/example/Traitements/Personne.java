package org.example.Traitements;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@Getter
public
class Personne
{
    @Setter
    private String prenom;
    private int age;
}