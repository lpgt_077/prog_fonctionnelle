package org.example.Traitements;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

public class Ex_9_3
{

    public static void main(String[] args) throws Exception {
        try
        {
            System.out.println("Working Directory = " + System.getProperty("user.dir"));
            Path path = Paths.get("src/main/java/org/example/Ressources/personnes.csv");

            List<Personne> personnes = Files.lines(path)
                    .skip(1)
                    .map(line -> {
                        String[] array = line.split(",");
                        String nom = array[0];
                        int age = Integer.parseInt(array[1]);
                        return new Personne(nom, age);
                    }).toList();

            Optional<Personne> younger = personnes.stream().filter(p -> p.getAge() == 21).findFirst();

            // comme "Optional" , on vérifie l'existence d 'une valeur de retour
           if (younger.isPresent())
           {
               System.out.println(younger.get());
           }
           else
           {
               System.out.println("Aucune valeur de retour !");
           }

        }
        catch (Exception e)
        {
           throw new Exception(e);
        }

    }


}
