package org.example.Traitements;
import java.util.List;
import java.util.function.Supplier;

public class Ex_6_2
{

    public static void main(String[] args)
    {
        Supplier<List<String>> months = () -> List.of("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");

        String res = months.get()
                .stream()
        // param "" --> Identity – an element that is the initial value of the reduction operation and the default result if the stream is empty
                .reduce("", (chaine, element) ->
                {
                    // chaine vide a la 1ere iteration
                    if (chaine.isEmpty())
                    {
                        chaine = element;
                    }
                    else if (element.length() < chaine.length())
                    {
                        return element;
                    }
                    // si length element > chaine
                    return chaine;


                });

        // 2ème méthode
        String res2 = months.get()
                .stream()
                // months.get().get(0) recup de la liste (supplier) --> recup du 1er element
                .reduce(months.get().get(0), (chaine, element) ->
                {

                    if (element.length() < chaine.length())
                    {
                        return element;
                    }
                    // si length element > chaine
                    return chaine;


                });

        System.out.println("Résultat : " + res);
       // System.out.println("Résultat : " + res2);


    }


}
