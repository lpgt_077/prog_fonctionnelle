package org.example.Traitements;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Ex_10_1
{

    public static void main(String[] args) throws Exception {
        try
        {
            System.out.println("Working Directory = " + System.getProperty("user.dir"));
            Path path = Paths.get("src/main/java/org/example/Ressources/personnes.csv");

            List<Personne> personnes = Files.lines(path)
                    .skip(1)
                    .map(line -> {
                        String[] array = line.split(",");
                        String nom = array[0];
                        int age = Integer.parseInt(array[1]);
                        return new Personne(nom, age);
                    }).toList();

            if (personnes.stream().allMatch(p -> p.getAge() >= 18))
            {
                System.out.println("Tous majeurs !");
            }
            else
            {
                System.out.println("Mineur détecté !");
            }





        }
        catch (Exception e)
        {
           throw new Exception(e);
        }

    }


}
