package org.example.Traitements;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Ex_12
{

    public static void main(String[] args) throws Exception {
        try
        {
            System.out.println("Working Directory = " + System.getProperty("user.dir"));
            Path path = Paths.get("src/main/java/org/example/Ressources/personnes.csv");

            int nb = (int) Files.lines(path)
                    .skip(1)
                    .map(line -> {
                        String[] array = line.split(",");
                        String nom = array[0];
                        int age = Integer.parseInt(array[1]);
                        return new Personne(nom, age);
                    })
                    .filter(p -> p.getAge() < 50)
                    .count();

            System.out.println("nb personnes mois de 50 ans : " + nb);

        }
        catch (Exception e)
        {
            throw new Exception(e);
        }

    }


}
