package org.example.Traitements;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

public class Ex_13_5
{

    public static void main(String[] args) throws Exception {
        try
        {
            System.out.println("Working Directory = " + System.getProperty("user.dir"));
            Path path = Paths.get("src/main/java/org/example/Ressources/matieres.csv");

          Map<String, Long> enseignementMap = Files.lines(path)
                    .skip(1)
                    .map(line -> {
                        String[] array = line.split(",");
                        String titre = array[0];
                        String ue = array[3];
                        int duree = Integer.parseInt(array[1]);
                        int coef = Integer.parseInt(array[2]);
                        return new Enseignement(titre, duree, coef, ue);
                    })
                  .collect(Collectors.groupingBy(Enseignement::getUe, Collectors.counting()));

            enseignementMap.entrySet().forEach(entry -> {
                System.out.println(entry.getKey() + " : " + entry.getValue());
            });


        }
        catch (Exception e)
        {
           throw new Exception(e);
        }

    }


}
