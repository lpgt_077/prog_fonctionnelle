package org.example.Traitements;
import java.util.List;
import java.util.function.Supplier;

public class Ex_5_2
{

    public static void main(String[] args)
    {
        Supplier<List<Integer>> nombres = () -> List.of(12, 9, 13, 4, 6, 2, 4, 12, 18);

        int somme = nombres.get()
                .stream()
        // param 0 --> Identity – an element that is the initial value of the reduction operation and the default result if the stream is empty
                // autre méthode (appel fct dans lambda) :  .reduce(0, (subtotal, element) -> sum(subtotal, element));
        .reduce(0, (subtotal, element) ->
        {
            System.out.println("subtotal : " + subtotal);
            System.out.println("element : " + element);
            return subtotal + element;
        });

        System.out.println("Résultat : " + somme);


    }

    private static Integer sum(Integer subtotal, Integer element)
    {
        System.out.println("subtotal : " + subtotal);
        System.out.println("element : " + element);
        return subtotal + element;
    }


}
