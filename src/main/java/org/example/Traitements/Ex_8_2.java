package org.example.Traitements;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Ex_8_2
{

    public static void main(String[] args) throws Exception {
        try
        {
            System.out.println("Working Directory = " + System.getProperty("user.dir"));
            Path path = Paths.get("src/main/java/org/example/Ressources/personnes.csv");

            List<Personne> personnes = Files.lines(path)
                    .skip(1)
                    .map(line -> {
                        String[] array = line.split(",");
                        String nom = array[0];
                        int age = Integer.parseInt(array[1]);
                        return new Personne(nom, age);
                    })
                            // comparaison de l'age permettant ensuite de trier par ordre croissant
                            .sorted((p1, p2) -> p1.getAge() - p2.getAge())
            .toList();

                   System.out.println(personnes);

        }
        catch (Exception e)
        {
           throw new Exception(e);
        }

    }


}
