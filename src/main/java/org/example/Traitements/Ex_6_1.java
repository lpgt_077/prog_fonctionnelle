package org.example.Traitements;
import java.util.List;
import java.util.function.Supplier;

public class Ex_6_1
{

    public static void main(String[] args)
    {
        Supplier<List<String>> months = () -> List.of("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");

        String res = months.get()
                .stream()
        // param "" --> Identity – an element that is the initial value of the reduction operation and the default result if the stream is empty
                .reduce("", (chaine, element) ->
                {
                    //System.out.println("chaine : " + chaine);
                    //System.out.println("element : " + element);
                    return chaine + element.substring(0,1) + "\n";
                });

        System.out.println("Résultat : " + res);


    }


}
