package org.example.Traitements;
import java.util.List;
import java.util.function.Supplier;

public class Ex_5_1
{

    public static void main(String[] args)
    {
        Supplier<List<Integer>> nombres = () -> List.of(12, 9, 13, 4, 6, 2, 4, 12, 18);

        int somme = nombres.get()
                .stream()
        // param 0 --> Identity – an element that is the initial value of the reduction operation and the default result if the stream is empty
                .reduce(0, (subtotal, element) -> subtotal + element);

        System.out.println("Résultat : " + somme);


    }


}
