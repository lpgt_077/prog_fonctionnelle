package org.example.Traitements;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Ex_7_2
{

    public static void main(String[] args)
    {
        Supplier<List<String>> months = () -> List.of("janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre");

        List<String> moisEnMajuscule = months.get()
                .stream()
                .filter(month -> month.contains("bre"))
                .map(month -> month.toUpperCase())
                .sorted()
                .collect(Collectors.toList());

        System.out.println("Mois : " + months.get());
        System.out.println("Res : " + moisEnMajuscule);


    }


}
