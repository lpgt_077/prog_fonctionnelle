package org.example.Traitements;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Ex_13_1
{

    public static void main(String[] args) throws Exception {
        try
        {
            System.out.println("Working Directory = " + System.getProperty("user.dir"));
            Path path = Paths.get("src/main/java/org/example/Ressources/matieres.csv");

            Files.lines(path)
                    .skip(1)
                    .map(line -> {
                        String[] array = line.split(",");
                        String titre = array[0];
                        String ue = array[3];
                        int duree = Integer.parseInt(array[1]);
                        int coef = Integer.parseInt(array[2]);
                        return new Enseignement(titre, duree, coef, ue);
                    })
                    .forEach(System.out::println);

        }
        catch (Exception e)
        {
           throw new Exception(e);
        }

    }


}
