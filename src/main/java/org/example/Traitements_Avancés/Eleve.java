package org.example.Traitements_Avancés;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@AllArgsConstructor
@Getter
public
class Eleve
{
    @Setter
    private String nom, prenom;
    private List<String> rattrages;
}