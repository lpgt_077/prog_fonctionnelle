package org.example.Traitements_Avancés;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ToString
@AllArgsConstructor
@Getter
public class CounterFunction implements Function<Stream<String>, Map<Integer, Integer>>
{

    @Override
    public Map<Integer, Integer> apply(Stream<String> stringStream)
    {
        // flatMap permet de fusionner en un seul stream les valeurs extraites depuis chaque ligne
        return stringStream.flatMap(line ->
                {
                    // on split chaque ligne puis on renvoie les valeurs voulues (col 5 à 10)
                    String array[] = line.split(";");
                    return Arrays.stream(array).skip(5).limit(5);
                })
                // params Collectors.toMap :
                //keyMapper – a mapping function to produce keys  (clé)
                // valueMapper – a mapping function to produce values  (valeur initiale pour chaque 1ere occurrence d'une valeur)
                // mergeFunction – a merge function,
                // used to resolve collisions between values associated with the same key --> Ici on additionne les valeurs
                // quand on rencontre plusieurs fois la mm clé, ce qui nous permet de compter le nb d'occurrences de chaque
                // valeur au final
                .collect(Collectors.toMap(Integer::parseInt, e -> 1, Integer::sum));

    }
}
