package org.example.Traitements_Avancés;

import org.example.Traitements.Enseignement;

import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ex_15_4
{


    public static void main(String[] args) throws Exception {
        try {
            String rattrapagesPath = "src/main/java/org/example/Ressources/rattrapages.csv";
            String matieresPath = "src/main/java/org/example/Ressources/matieres.csv";


            TextFileReader textRattrapages = new TextFileReader(rattrapagesPath);
            TextFileReader textMatieres = new TextFileReader(matieresPath);

            // matieres a rattraper
            List<String> rattrapages = textRattrapages.get().flatMap(l -> {
                String array[] = l.split(",");
                return Arrays.stream(array[2].replaceAll("\"", "").split(";"));
            }).distinct().sorted().toList();


            List<Eleve> eleves = textRattrapages.get().map(l -> {
                String array[] = l.split(",");
                return new Eleve(array[0], array[1], Arrays.asList(array[2].replaceAll("\"", "").split(";")));
            }).toList();

            // matieres
            Stream<String> matieres = textMatieres.get().map(l -> {
                String array[] = l.split(",");
                return array[0];
            });
/*
            matieres.map(m -> {
                        Map<String, List<String>> map2 = new HashMap<>();
                        map2.put(m, eleves.stream().filter(eleve -> eleve.getRattrages().contains(m)).map(e -> e.getNom()).toList());
                        return map2;
                    })
                    .flatMap(map -> map.entrySet().stream())
                    .forEach(x -> System.out.println(x.getKey() + " : " + x.getValue().size() + " personnes"));
            */
// methode trop compliquée car pas besoin d'associer chaque matiere avec la liste des eleves concernés
             matieres.map(m -> {
                Map<String, Integer> map2 = new HashMap<>();
                map2.put(m, eleves.stream().filter(eleve -> eleve.getRattrages().contains(m)).map(e -> e.getNom()).toList().size());
                return map2;
            })
                     .flatMap(map -> map.entrySet().stream())
                     .forEach(x -> System.out.println(x.getKey() + " : " + x.getValue() + " personnes"));


            // methode correction : faire un flat map a partir de la liste des rattrapages et compter le nb
            // de fois qu'apparait chaque matiere dans la liste
            Map<String, Long> matieresCorrection = eleves.stream()
                    .flatMap(x -> x.getRattrages().stream())
                    // function.identity sert a reprendre la valeur en input : ici la matiere
                    // on peux la remplacer la lambda suivante : str -> str
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));


          System.out.println("correction : " + matieresCorrection);

        }
        catch (Exception e)
        {
            throw new Exception(e);
        }

    }


}
