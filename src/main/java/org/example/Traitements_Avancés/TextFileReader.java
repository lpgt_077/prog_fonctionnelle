package org.example.Traitements_Avancés;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

@ToString
@AllArgsConstructor
@Getter
public class TextFileReader implements Supplier<Stream<String>>
{

    private String filePath;

    @Override
    public Stream<String> get() {
        try
        {
            Path path = Paths.get(filePath);

            Stream<String> lignes = Files.lines(path)
                                          .skip(1);

            // check permettant de renvoyer une valeur nulle si le Stream est vide sans le consommer
            return Optional
                    .ofNullable(lignes)
                    .orElseGet(Stream::empty);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e.toString());
        }
    }
}
