package org.example.Traitements_Avancés;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@AllArgsConstructor
@Getter
public
class Commentaire
{
    @Setter
    private String id_video, comment;
    private int like, replies;
}