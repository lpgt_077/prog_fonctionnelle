package org.example.Traitements_Avancés;

import java.util.Arrays;
import java.util.List;

public class Ex_15_3
{


    public static void main(String[] args) throws Exception {
        try
        {
            String rattrapagesPath = "src/main/java/org/example/Ressources/rattrapages.csv";
            String matieresPath = "src/main/java/org/example/Ressources/matieres.csv";




            TextFileReader textRattrapages = new TextFileReader(rattrapagesPath);
            TextFileReader textMatieres = new TextFileReader(matieresPath);

            // matieres a rattraper
            List<String> rattrapages = textRattrapages.get().flatMap(l -> {
                String array[] = l.split(",");
                return Arrays.stream(array[2].replaceAll("\"", "").split(";"));
            }).distinct().sorted().toList();

            List<String> matieres = textMatieres.get().map(l -> {
            String array[] = l.split(",");
            return array[0];
        }).toList();

            System.out.println("rattrapages : " + rattrapages);

            System.out.println("\n matieres non rattrapés : ");
            // matiere non rattrapées
            matieres.stream().filter(matiere -> !rattrapages.contains(matiere)).forEach(System.out::println);





        }
        catch (Exception e)
        {
            throw new Exception(e);
        }

    }


}
