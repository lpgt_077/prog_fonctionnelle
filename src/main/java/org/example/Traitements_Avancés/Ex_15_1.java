package org.example.Traitements_Avancés;

import java.util.Arrays;
import java.util.stream.Stream;

public class Ex_15_1
{


    public static void main(String[] args) throws Exception {
        try
        {
            String filePath = "src/main/java/org/example/Ressources/rattrapages.csv";
            TextFileReader textReader = new TextFileReader(filePath);
            Stream<Eleve> eleves = textReader.get().map(l -> {
                String array[] = l.split(",");
                return new Eleve(array[0], array[1], Arrays.asList(array[2].replaceAll("\"", "").split(";")));
            });

           eleves.forEach(System.out::println);


        }
        catch (Exception e)
        {
            throw new Exception(e);
        }

    }


}
