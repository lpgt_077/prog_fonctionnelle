package org.example.Traitements_Avancés;


public class Ex_14_4
{


    public static void main(String[] args) throws Exception {
        try
        {
            String filePath = "src/main/java/org/example/Ressources/euromillions_201902.csv";
            TextFileReader textReader = new TextFileReader(filePath);

            CounterFunction counter = new CounterFunction();
            MapWriter writer = new MapWriter ("src/main/resources/euromillion_stats.txt");

            writer.accept (
                    counter.apply (
                            textReader.get()
                    )
            );

        }
        catch (Exception e)
        {
            throw new Exception(e);
        }

    }


}
