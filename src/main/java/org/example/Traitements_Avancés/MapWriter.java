package org.example.Traitements_Avancés;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.function.Consumer;

@ToString
@AllArgsConstructor
@Getter
class MapWriter implements Consumer<Map<Integer, Integer>>
{
    private String outputFile;

    @Override
    public void accept(Map<Integer, Integer> map)
    {

        /*
        TODO
        On peut aussi s'assurer que les dossiers intermédiaires du
        chemin existent avec Files.createDirectories() et Path.getParent()
         */
        Path path = Paths.get(outputFile);

        String chaine = map.entrySet().stream()
                .map(entry -> {
                    return entry.getKey() + " -> " + entry.getValue();
                }).reduce("", (subtotal, element) ->
                {
                    return subtotal + element + '\n';
                });

        try {
            Files.write(path, chaine.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
