package org.example.Traitements_Avancés;


public class Ex_14_3
{


    public static void main(String[] args) throws Exception {
        try
        {
            String filePath = "src/main/java/org/example/Ressources/euromillions_201902.csv";
            TextFileReader textReader = new TextFileReader(filePath);

            CounterFunction counter = new CounterFunction();
            System.out.println("Nombre d'occurrences pour chaque numéro :");
            counter.apply(textReader.get()).entrySet().forEach(entry -> {
                System.out.println(entry.getKey() + " : " + entry.getValue());
            });

        }
        catch (Exception e)
        {
            throw new Exception(e);
        }

    }


}
