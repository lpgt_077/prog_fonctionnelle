package org.example.Traitements_Avancés;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Ex_16
{


    public static void main(String[] args) throws Exception {
        try {
            String commentsPath = "src/main/java/org/example/Ressources/GBcomments.csv";
            TextFileReader textComments = new TextFileReader(commentsPath);
    //video_id,comment_text,likes,replies
           Map<String, Long> comments = textComments.get()
                   // on filtre directement sur la ligne entiere pour ne pas avoir a splitter, sachant que les autres
                   // colonnes (id, etc) ne peuvent pas contenir accidentellement le mot recherché
                   .filter(l -> l.toLowerCase().contains("iphone"))
                   .map(l -> {
                String array[] = l.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
               // return new Commentaire(array[0], array[1], Integer.parseInt(array[2]), Integer.parseInt(array[3]) );
               Map<String, String> map2 = new HashMap<>();
               map2.put(array[0], array[1]);
               return map2;
            }).flatMap(map -> map.entrySet().stream())
                   .collect(Collectors.groupingBy(m -> m.getKey(), Collectors.counting()));

                  comments.entrySet()
                          .stream()
                          // on trie les valeurs , ordre inversé pour obtenir des valeurs décroissantes
                          .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                          // on ne garde ques les 10 premieres valeurs
                          .limit(10)
                          .forEach(System.out::println);

            //System.out.println(comments);

        }
        catch (Exception e)
        {
            throw new Exception(e);
        }

    }


}
