package org.example.Traitements_Avancés;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Ex_14
{

    public static void main(String[] args) throws Exception {
        try
        {
            System.out.println("Working Directory = " + System.getProperty("user.dir"));
            Path path = Paths.get("src/main/java/org/example/Ressources/euromillions_201902.csv");

            Files.lines(path)
                    // on ignore les en-têtes
                .skip(1)
                .map(line ->
                {
                    String array[] = line.split(";");
                    // on transforme l'array contenant les valeurs en stream
                    // on passe les 5 premieres colonnes et on prend seulement les 5 suivantes
                    return Arrays.stream(array).skip(5).limit(5).collect(Collectors.joining(","));
                })
                .forEach(System.out::println);

        }
        catch (Exception e)
        {
            throw new Exception(e);
        }

    }


}
