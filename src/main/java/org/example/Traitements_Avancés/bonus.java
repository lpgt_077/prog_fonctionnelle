package org.example.Traitements_Avancés;

import org.example.Traitements.Personne;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class bonus
{

    public static void main(String[] args) throws Exception {
        try
        {
            System.out.println("Working Directory = " + System.getProperty("user.dir"));
            Path path = Paths.get("src/main/java/org/example/Ressources/personnes.csv");
            // bonus hors exo
            List<Personne> personnes =
                    Files.lines(path)
                            .skip(1)
                            .map(line -> {
                                String[] array = line.split(",");
                                String nom = array[0];
                                int age = Integer.parseInt(array[1]);
                                return new Personne(nom, age);
                            }).collect(Collectors.toList());


            Map<Integer, List<Personne>> personneMap = personnes.stream().collect(Collectors.groupingBy(Personne::getAge));
            Map<Integer, List<Personne>> personneMap2 = personnes.stream().collect(Collectors.groupingBy(P -> P.getPrenom().length()));

            personneMap.forEach((key, value) -> System.out.println(key));
            personneMap2.entrySet().stream().forEach(x -> System.out.println(x.getKey() + " : " + x.getValue().size() + " personnes"));
            System.out.println(personneMap2);

            // on ne peux pas filtrer et opérer d'autres traitements avec le forEach de la ligne precedente
            // a la difference du stream() ci dessous
            personneMap.entrySet().stream().forEach(x -> System.out.println(x.getKey() + " : " + x.getValue().size() + " personnes"));

            Optional<Integer> min = personneMap.keySet().stream().min(Integer::compareTo);

            // on recupere les valeurs avec values, valueSet n'existe pas car doublons possibles dans les valeurs
            personneMap.values().stream().forEach(l -> l.stream().forEach(p -> p.setPrenom(p.getPrenom().toUpperCase())));

            System.out.println(personneMap);
        }
        catch (Exception e)
        {
            throw new Exception(e);
        }

    }
}
