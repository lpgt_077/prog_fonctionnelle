package org.example.Traitements_Avancés;

import java.util.Arrays;
import java.util.stream.Stream;

public class Ex_15_2
{


    public static void main(String[] args) throws Exception {
        try
        {
            String filePath = "src/main/java/org/example/Ressources/rattrapages.csv";
            TextFileReader textReader = new TextFileReader(filePath);
           textReader.get().flatMap(l -> {
            String array[] = l.split(",");
            return Arrays.stream(array[2].replaceAll("\"", "").split(";"));
        }).distinct().sorted().forEach(System.out::println);





        }
        catch (Exception e)
        {
            throw new Exception(e);
        }

    }


}
